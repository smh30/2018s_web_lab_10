package ictgradschool.web.lab10;

public class User {
    private String firstName;
    private String lastName;
    private String country;
    private String city;

    public User (String fname, String lname, String country, String city) {
        firstName = fname;
        lastName = lname;
        this.country = country;
        this.city = city;
    }

    public String getFirstName (){
        return firstName;
    }

    public String getLastName (){
        return lastName;
    }

    public String getCountry(){
     return country;
    }

    public String getCity(){
        return city;
    }
}
