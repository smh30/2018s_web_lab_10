package ictgradschool.web.lab10.exercise01;

import ictgradschool.web.lab10.User;
import ictgradschool.web.lab10.utilities.HtmlHelper;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class UserDetailsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession(true);

        User user = new User(request.getParameter("fname"), request.getParameter("lname"), request.getParameter("country"), request.getParameter("city"));

        session.setAttribute("user", user);

        Cookie fname = new Cookie("fname", request.getParameter("fname"));
        Cookie lname = new Cookie("lname", request.getParameter("lname"));
        Cookie country = new Cookie("country", request.getParameter("country"));
        Cookie city = new Cookie("city", request.getParameter("city"));

        response.addCookie(fname);
        response.addCookie(lname);
        response.addCookie(country);
        response.addCookie(city);

        User currentUser = (User)session.getAttribute("user");

        // Header stuff
        out.println(HtmlHelper.getHtmlPagePreamble("Web Lab 10 - Sessions"));

        out.println("<a href=\"index.html\">HOME</a><br>");

        out.println("<h3>Data entered: </h3>");
        out.println("<ul>");
        out.println("<li>First Name: " + currentUser.getFirstName() +"</li>");
        out.println("<li>Last Name:  " + currentUser.getLastName() +"</li>");
        out.println("<li>Country: " + currentUser.getCountry() +" </li>");
        out.println("<li>City: " + currentUser.getCity() +" </li>");
        out.println("</ul>");

        out.println(HtmlHelper.getHtmlPageFooter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Process POST requests the same as GET requests
        doGet(request, response);
    }
}
