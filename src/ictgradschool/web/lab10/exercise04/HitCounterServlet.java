package ictgradschool.web.lab10.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("true".equalsIgnoreCase(req.getParameter("removeCookie"))) {


            for (Cookie co : req.getCookies()) {
                if (co.getName().equals("hits")) {
                    co.setMaxAge(0);
                }
                resp.addCookie(co);
            }

        } else {
            //get all teh cookies (at first stage none exist tho)
            Cookie[] cookies = req.getCookies();
            boolean exists = false;

            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("hits")) {
                        int c = Integer.valueOf(cookie.getValue());
                        c++;
                        cookie.setValue("" + c);
                        resp.addCookie(cookie);
                        exists = true;
                    }
                }
                if (exists == false) {
                    Cookie c = new Cookie("hits", "1");
                    resp.addCookie(c);
                }
                // if it's the first time there's no cookie yet so create it and set to 1
                //problem at the moment: if _some_ cookies already exist it won't create "hits"
                //so need to see if (there's not cookie called hits) rather than (there's not cookies)
                // added boolean "exists" to fix that

            } else {
                Cookie c = new Cookie("hits", "1");
                resp.addCookie(c);
            }

        }


        resp.sendRedirect("hit-counter.html");

    }
}
